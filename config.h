//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Command*/		/*Update Interval*/	/*Update*/
	{"", "cat /tmp/recordingicon 2>/dev/null",	0,	1},
	{"",	"sb-music",	5,	2},
	// {"",	"sb-progbar",	5,	3},
	{"",	"sb-tasks",	3,	4},
	{"",	"sb-pacpackages",	0,	5},
	// {"",	"sb-clickbible",		0,	6},
	// {"",	"sb-torrent",	20,	7},
	{"",	"sb-news",		0,	8},
	// {"",	"sb-price btc Bitcoin '[BTC]' gbp",				1800,	9},
	// {"",	"sb-price xmr Monero '[XMR]' gbp",				1800,	10},
	//{"",	"sb-price bat \"Basic Attention Token\" '[BAT] '",	1800,	11},
	//{"",	"sb-price lbc \"LBRY Token\" 📚",			1800,	12},
	{"",	"sb-weather",	5000,	13},
	{"",	"sb-mailbox",	2,	14},
	{"",	"sb-volume",	0,	15},
	{"",	"sb-battery",	5,	16},
	{"",	"sb-battime",   10,	17},
	// {"",	"sb-disk /home",	60,	18},
	// {"",	"sb-memory",	10,	19},
	{"",	"sb-cpu",		1,	20},
	{"",	"sb-fan",		1,	25},
	{"",	"sb-cpubars",		1,	21},
	{"",	"sb-nettraf",	1,	22},
	{"",	"sb-internet",	1,	23},
	{"",	"sb-clock",	30,	24},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
//static char *delim = "   ";
static char *delim = " │ ";

// Have dwmblocks automatically recompile and run when you edit this file in
// vim with the following line in your vimrc/init.vim:

// autocmd BufWritePost ~/.local/src/dwmblocks/config.h !cd ~/.local/src/dwmblocks/; sudo make install && { killall -q dwmblocks;setsid dwmblocks & }
